#!/bin/bash
# Condense and merge time sperated data files
# ===========================================
# Quick example of using CDO to do (monthly) time averaging 
# and merging data files containing the same meteorological 
# parameters along the time axis. 

datadir=/home/ubuntu/era5
metprms=(northward_wind_at_10_metres eastward_wind_at_10_metres)

cd $datadir
for metprm in ${metprms[@]}  ; do
    dfiles=$( ls ${metprm}_*.nc )   
    for datafile in ${dfiles[@]} ; do                      
	   cdo monmean ${dfile} monmean_${dfile}.nc
    done
    mfiles=$( ls monmean_${metprm}_*.nc )
    cdo mergetime ${mfiles[@]} ${metprm}_monmean_era5.nc
done




