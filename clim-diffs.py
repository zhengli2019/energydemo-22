''' Relative Climatology Differences
    ================================
    Define a set of functions that then allow for different processing options 
    1) Consider individual simulations with the idea to pick
       individual simulations apart first, rather than naïvely loop
       over a bunch of simulations and then work backwards for
       explanations when our results don't match intuitions. Output is
       a panel plot of relative differences between monthly
       climatologies.
    2) Calculate ensemble statistics for given parameters
    3) Option to do the above, but for annual mean stats
                                                                             '''
import matplotlib.pyplot as plt             
import matplotlib as mpl
import xarray as xr
import pandas as pd
import numpy as np
import cartopy
import sys
import glob as grod
import cftime

from cartopy.io.img_tiles import Stamen
from cdo import *
cdo = Cdo()

metprm = sys.argv[1]  # e.g. 'pr'
ens_range = sys.argv[2]

''' Along with libraries above will pull in some possible fancy map 
    backgrounds (each time draw a map the program makes and online quiery to 
    pull in the image, so can be a bit slow).  
                                                                             '''
stamen_terrain = Stamen('terrain-background') 
stamen_toner   = Stamen('toner-background')

# ==:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:==

def get_year(x):
    '''
    Recognize the type of datetime object used in time index for different
    simulations so can use the correct routines to read the years covered 
    in the simulations.
    
    Variables
    ---------
    x : a datetime element from a simulation index
    '''
    
    if type(x) == cftime._cftime.DatetimeNoLeap:
        return x.year
    elif type(x) == cftime._cftime.Datetime360Day:
        return x.year		
    else:
        return pd.to_datetime(x).year

def set_values(hsim_path,psim_path,metprm,calc_annual=False) :
    ''' 
    Read in files and calculate tables of relative differences that can 
    be passed to plotting routines. 

    Need to pick years out of what we have access to that we want to make 
    a climatology from. Should use the same number of years in both for 
    consistancy (if not need to be very careful about how significance of 
    change is computed to account for the uncertainy in the means being 
    different). 
    ** Here we have hard coded the periods between 1976:2005 and 
       2021:2050. This should be made a free parameter in futre. ** 

    Variables
    ---------
    hsim_path   : path to historical simulation (str)
    psim_path   : path to projection simulation (str)
    metprm      : name of meteorological parameter being investigated (str)
    calc_annual : report annual means instead of monthly (bool)
    '''

    ## Read in data
    H = xr.open_dataset(hsim_path)
    P = xr.open_dataset(psim_path)

    ## Change units
    if metprm == 'pr' : 
        H.pr.values = 86400*H.pr.values
        P.pr.values = 86400*P.pr.values
        H.pr.attrs['units'] = 'mm/month'
        P.pr.attrs['units'] = 'mm/month'
    if metprm == 'tasmax':
       if H.tasmax.values.max() > 200 :
           H.tasmax.values = H.tasmax.values - 273.15
       if P.tasmax.values.max() > 200 :
           P.tasmax.values = P.tasmax.values - 273.15
       H.tasmax.attrs['units'] = 'Celsius Degree'
       P.tasmax.attrs['units'] = 'Celsius Degree'

    ## Monthly Climatologies
    idx_clim = [get_year(x) 
                in range(1976,2006) 
                for x in H.time.values[:]]
    H = H.sel(time=idx_clim)

    idx_clim = [get_year(x)
                in range(2021,2051) 
                for x in P.time.values[:]]
    P = P.sel(time=idx_clim)

    if calc_annual : 
        nyrs = len(
            np.unique(
                [get_year(x) for x in H.time.values]
            ))
        H_clim = H.mean('time')
        H_clim['StndDev'] = H.std('time')[metprm]
        H_clim['StndErr'] = H_clim['StndDev']/np.sqrt(nyrs)
        nyrs = len(
            np.unique(
                [get_year(x) for x in P.time.values]
            ))
        P_clim = P.mean('time')
        P_clim['StndDev'] = P.std('time')[metprm]
        P_clim['StndErr'] = P_clim['StndDev']/np.sqrt(nyrs)

    else : 
        nyrs = len(
            np.unique(
                [get_year(x) for x in H.time.values]
            ))
        H_clim = H.groupby('time.month').mean('time')
        H_clim['StndDev'] = H.groupby('time.month').std('time')[metprm]
        H_clim['StndErr'] = H_clim['StndDev']/np.sqrt(nyrs)
        nyrs = len(
            np.unique(
                [get_year(x) for x in P.time.values]
            ))
        P_clim = P.groupby('time.month').mean('time')
        P_clim['StndDev'] = P.groupby('time.month').std('time')[metprm]
        P_clim['StndErr'] = P_clim['StndDev']/np.sqrt(nyrs)

    ''' Once the units are standarized and climatologies have been constructed,
        now calculate the relative difference between the climatology
        means in the projection and the 'base line' values. As well,
        can can calculate how many sigma levels we need to exapand the
        standard errors of those means in order to get the raw
        differences to overlap, which here functions as an addhoc
        probability of the differences being significant.  
        ** Would be better to estimate this through bootstraping, 
           and maybe to do the same with the standard deviation values
           as well. **
                                                                             '''

    D = H_clim.copy(deep=True)                   # create a new copy of grid
    D = D.drop([metprm,'StndDev','StndErr'])     # make it an empty copy

    D['raw_diff'] = (                            # diff climatologies
        P_clim[metprm] - H_clim[metprm] )
    D['rel_diff'] = D.raw_diff/H_clim[metprm]    # diff in mean
                                                 # ... as % of historical
    
    D['dev_diff'] = (                            # diff in variation
        P_clim.StndDev - H_clim.StndDev )        # ... as % of historical
    D['rel_dev'] = D.dev_diff/H_clim.StndDev

    ''' Record how many stnd err devs between the climatology means '''       
    D['sig_diff'] = D.raw_diff.copy(deep=True)   
    D['sig_diff'].values = (D.sig_diff.values*0)+4

    if calc_annual :
        for sig_lev in range(4,0,-1) : 
            D['sig_diff'].values[
                np.abs(D.raw_diff) 
                <= (sig_lev*P_clim.StndErr
                    + sig_lev*H_clim.StndErr)] = sig_lev
    else : 
        for m in range(1,13) : 
            for sig_lev in range(4,0,-1) : 
                D['sig_diff'].sel(month=m).values[
                    np.abs(D.raw_diff.sel(month=m)) 
                    <= (sig_lev*P_clim.StndErr.sel(month=m)
                        + sig_lev*H_clim.StndErr.sel(month=m))] = sig_lev
        

    return(D)

def regrid(data,scheme,griddes_file) :
    '''
    Interpolate a given data set onto a predefined grid
    * Current setting lets us use the CDO convince function but makes the 
      implementation clunky since it leans a lot on creating external 
      files and running an extranl (linux only), analysis program. 
      Would be nice to work out the math using just xarray and 
      other internal libraries. 
    * Routine deletes the temporary files created to reduce the function's 
      external effects (even though in practice often end up saving the output 
      after calling the function). 
    * One hassle this function works through is that even though immediatly
      load the values and delete the hard copy file, the output file still has
      to have a unique name to avoid having the differnet variables "point" 
      towards the wrong data-shadow. 
    * Need to have an externally stored file, in CDO expected format, 
      describing the target grid. 
    
    Variables
    ---------
    X : data set for regridding (xarray)
    scheme : cdo name for remapping routine, e.g. "nn" (str)
    griddes_file : path to file describing the desired grid (str)
    '''
    
    tag = pd.datetime.now().strftime('%Y%m%d%H%M%S%f')
    data.to_netcdf('tmp0.nc')
    if scheme == 'nn' :
        cdo.remapnn(griddes_file,
                  input = 'tmp0.nc', #'tmp0.nc',
                  output = 'tmp'+tag+'.nc',
                  options = '-f nc')
        
    #| -> ADD OTHER FUNCTION LABELS HERE <-          |#
    #|    ------------------------------             |#
    #|    Or, can do something fancy where call the  |#
    #|    function using its name described with a   |#
    #|    string                                     |#
    
    else :
        print('+++ Unknown remapping scheme +++')
        return()

    data = xr.open_dataset('tmp'+tag+'.nc')
    [os.remove(x) for x in ['tmp0.nc','tmp'+tag+'.nc']]
    return(data)

def show_range(D,latlons=[]) :
    '''
    Examine the range of values in the difference statistics
    Return a matrix (array) with statistics relevant to choosing
    a range for plot color bars, etc. 

    Variables
    ---------
    D : data file containing difference statistics  (xarray)
    latlons : optional list giving a subregion to check
              [min_lat, max_lat, min_lon, max_lon]  (list of floats)
    '''
    if metprm == 'tasmax':
        mean_diff = 'raw_diff'
    else:
        mean_diff = 'rel_diff' 
    
    if latlons:
        min_lat =  latlons[0] #D.lat.values.min()
        max_lat =  latlons[1] #D.lat.values.max()
        min_lon =  latlons[2] #D.lon.values.min()
        max_lon =  latlons[3] #D.lon.values.max()
        min_mean_diff = np.round(
            D.val_diff.where(
                (D.lat>min_lat)
                & (D.lat<max_lat)
                & (D.lon<max_lon)
                & (D.lon>min_lon),
                drop=True
            ).min().values,decimals=2)*100
        max_mean_diff=np.round(
            D.val_diff.where(
                (D.lat>min_lat)
		& (D.lat<max_lat)
                & (D.lon<max_lon)
                & (D.lon>min_lon),
		drop=True
            ).max().values,decimals=2)*100
        min_stnd_dev=np.round(
            D.dev_diff.where(
                (D.lat>min_lat)
                & (D.lat<max_lat)
                & (D.lon<max_lon)
                & (D.lon>min_lon),
                drop=True
            ).min().values,decimals=2)*100
        max_stnd_dev=np.round(
            D.dev_diff.where(
                (D.lat>min_lat)
                & (D.lat<max_lat)
                & (D.lon<max_lon)
                & (D.lon>min_lon),
		drop=True
            ).max().values,decimals=2)*100
        mean_mean_diff=np.round(
            D.val_diff.where(
                (D.lat>min_lat)
		& (D.lat<max_lat)
                & (D.lon<max_lon)
                & (D.lon>min_lon),
		drop=True
            ).mean().values,decimals=2)*100
    else:
        min_mean_diff=np.round(D.val_diff.min().values,decimals=2)*100
        max_mean_diff=np.round(D.val_diff.max().values,decimals=2)*100
        min_stnd_dev=np.round(D.dev_diff.min().values,decimals=2)*100
        max_stnd_dev=np.round(D.dev_diff.max().values,decimals=2)*100
        mean_mean_diff=np.round(D.val_diff.mean().values,decimals=2)*100
    print('---------------------------------------')
    print('min mean diff (as %): ',
          str(min_mean_diff),'%')
    print('max mean diff (as %): ',
          str(max_mean_diff),'%')
    print('mean mean diff (as %): ',
          str(mean_mean_diff),'%')
    print()
    print('min stnd.dev. diff (as %): ',
          str(min_stnd_dev),'%')
    print('max stnd.dev. diff (as %): ',
          str(max_stnd_dev),'%')
    print('---------------------------------------')
    
    return np.array([min_mean_diff,max_mean_diff,min_stnd_dev,max_stnd_dev])

def map_simulation_values(D,fname_hsim,latlons=[],show_dev=True) :
    '''
    Draw estimated statistics on a map.

    Variables
    ---------
    D : data file containing statistics to be plot (xarray)
    fname_hsim : name simulation being described, 
                 as described by the historical data
                 file header                       (str)
    latlons : optional list giving a subregion to check
              [min_lat, max_lat, min_lon, max_lon]  (list of floats)
    show_dev : whether want to draw variability contours over map (bool)
    '''
    
    ## Use different color schemes for different variables
    if metprm == 'pr' :
    	color_map = 'PuOr'
    	variable_name = 'Precipitation'
    	scatter_name = ' Percent Change (%)'
    	contour_name = ' Standard Deviation Change (%)'
    elif metprm =='tasmax':
    	color_map = 'RdYlBu_r'
    	variable_name = 'Maximum Temperature'
    	contour_name = ' Standard Deviation Change (%)'
    elif metprm == 'sfcWind':
        color_map = 'RdYlBu_r'
        variable_name = 'Mean Wind Speed '
        scatter_name = 'Mean Percent Change (%)'
        contour_name = 'Standard Deviation Change (%)'
        mean_range = [-5,5]
    else :
    	color_map = 'RdYlBu_r' 
    	variable_name = 'Max Wind Speed' 
    	contour_name = ' Standard Deviation Change (%)'
 
    ## Set region  
    if latlons : 
        min_lat = latlons[0]
        max_lat = latlons[1]
        min_lon = latlons[2]
        max_lon = latlons[3]
    else : 
        min_lat = D.lat.values.min()
        max_lat = D.lat.values.max()
        min_lon = D.lon.values.min()
        max_lon = D.lon.values.max()
        
    clon = min_lon + (max_lon - min_lon)/2.0
    clat = min_lat + (max_lat - min_lat)/2.0 
    
    ## Set subploting parameters
    if calc_annual :
        nr = 1
        nc = 1
        ne = 1
        wide = 24
        fig_name = 'Annual '
    else :
        month = range(1,13)
        nr = 3
        nc = 4
        ne = 12
        wide = 28
        fig_name = 'Monthly '
        month_lab=['January','February', 'March','April',
                   'May','June','July','August',
                   'September','October','November','December']
        
    print(variable_name,contour_name,scatter_name)

    ## Create plotting framework
    fig,axs = plt.subplots( 
        frameon=False,
        nrows=nr,
        ncols=nc,
        figsize=(wide, 18),
        subplot_kw={'xticks': [], 'yticks': []}) 

    fig.subplots_adjust(    
        left=0.01, right=0.90,
        top =0.95, bottom=0.05,
        hspace=0.06, wspace=0.05)
    
    if calc_annual :
        for i in range (0,nr):              
            for j in range (0,nc):
                axs.spines['left'].set_visible(False)
                axs.spines['right'].set_visible(False)
                axs.spines['bottom'].set_visible(False)
                axs.spines['top'].set_visible(False)
    else : 
        for i in range (0,nr):              
            for j in range (0,nc):
                axs[i,j].spines['left'].set_visible(False)
                axs[i,j].spines['right'].set_visible(False)
                axs[i,j].spines['bottom'].set_visible(False)
                axs[i,j].spines['top'].set_visible(False)
    
    ## Draw maps
    for i in range(0,ne):
        chart = fig.add_subplot(  
            nr, nc, i+1,projection=cartopy.crs.LambertConformal(
                central_longitude=clon,central_latitude=clat))

        _ = chart.set_extent(
            [min_lon, max_lon, min_lat, max_lat], 
            crs=cartopy.crs.PlateCarree())
        _ = chart.coastlines('50m') 

        if calc_annual :
        ## Add scatter plot with relative difference (color)
        ## and significant SD level (size)  
            m0 = chart.scatter(                  
                D.lon,D.lat,
                transform=cartopy.crs.PlateCarree(),
                cmap=color_map,    
                c=D.val_diff*100,
                s=D.sig_diff*120,    
                vmin=min(mean_range),vmax=max(mean_range))
        else:
            m0 = chart.scatter(                  
                D.lon,D.lat,
                transform=cartopy.crs.PlateCarree(),
                cmap=color_map,    
                c=D.val_diff.sel(month=month[i])*100,
                s=D.sig_diff.sel(month=month[i])*5,    
                vmin=min(mean_range),vmax=max(mean_range)) 
        
        if show_dev : 
            if calc_annual : 
                rd = D.dev_diff*100
            else : 
                rd = D.dev_diff.sel(month=month[i])*100
            m1 = rd.plot.contour(
                'lon','lat',ax=chart,
                levels=np.arange(-15,5,5),
                linewidths=4,linestyles='dashed',
                transform=cartopy.crs.PlateCarree(),
                add_colorbar=False,
                vmin=-12,vmax=0,
                alpha=0.9,cmap='Greys_r')
            m2 = rd.plot.contour(
                'lon','lat',ax=chart,
                levels=np.arange(0,20,5),
                linewidths=4,linestyles='solid',
                transform=cartopy.crs.PlateCarree(),
                add_colorbar=False,
                vmin=0,vmax=12,
                alpha=0.9,cmap='Greys')
        if not calc_annual :
            _ = plt.title(month_lab[i],fontsize=14)

    ## Annotate maps
    fig.suptitle('_'.join(fname_hsim.split('_')[1:5])
               		+'_'+'_'.join(fname_hsim.split('_')[6:9])
			, fontsize=16)
    plt.colorbar(
        m0, cax = plt.axes([0.91, 0.05, 0.015, 0.9]),
        label=variable_name+scatter_name)
    
    if show_dev : 
        ## Add two contour lines colorbar
        bounds=np.arange(-15,5,5)
        cmap=plt.get_cmap('Greys_r',len(bounds))
        norm=mpl.colors.BoundaryNorm(bounds,len(bounds))
        ax_coutour=fig.add_axes([0.96, 0.05, 0.015, 0.45])
        mpl.colorbar.ColorbarBase(ax_coutour,cmap = cmap,
    	    norm=norm)
        bounds=np.arange(0,20,5)
        cmap=plt.get_cmap('Greys',len(bounds))
        norm=mpl.colors.BoundaryNorm(bounds,len(bounds))
        ax_coutour=fig.add_axes([0.96, 0.5, 0.015, 0.45])
        mpl.colorbar.ColorbarBase(
            ax_coutour,cmap = cmap,norm=norm)
        ## Add text on figures to explain contour lines
        plt.figtext(
            0.99,0.6,variable_name+contour_name,
            fontsize=10,rotation='vertical')
        explain = (
            '----- Changes of Standard deviation < 0, '
            + '───── Changes of Standard deviation > 0')
        plt.figtext(
            0.35,0.03,explain,fontsize=14,
            rotation='horizontal')
        
    plt.savefig(
        '_'.join(fname_hsim.split('_')[1:5])
        +'_'+'_'.join(fname_hsim.split('_')[6:9])
	+'.png')
    plt.close()


def map_ensemble_values(
        D,R,latlons,metprm,show_dev=True,
        sim_name='',calc_annual=False) :
    '''
    Draw estimated ensemble statistics on a map.
    ** Currently draws two maps, should seperate 
       these into two seperate functions **
    1) Diagram of ensemble mean percentage change, 
       with scatter point size representitive of 
       statistical significance of the chanange,
       and optional contour lines marking the 
       ensemble standard deviation.
    2) A plot of the ensemble standard deviation,
       with hatching representing degree ensemble members
       agree with each other on the direction of change. 

    Variables
    ---------
    D : data file containing statistics to be plot (xarray)
    R : data range
    show_dev : whether to show variability around the 
               climatology mean (bool)
    sim_name : plot title (str)
    calc_annual : whether to show annual 
                  rather than monthly stats (bool)
    '''
    
    ## Only save interger for max and min mean value and max SD values
    if metprm =="tasmax":
        min_mean_diff = np.round(R[0]/100,decimals=1)
        max_mean_diff = np.round(R[1]/100,decimals=1)
        max_stnd_dev = np.round(R[-1]/100,decimals=1)
    else:
        min_mean_diff,max_mean_diff,min_stnd_dev,max_stnd_dev = np.round(R,decimals=1)
        
    print(min_mean_diff,max_mean_diff,max_stnd_dev)
    
    ## Select upper and lower boundary of scatter colorbar
    scatter_upper=round(max(abs(min_mean_diff),max_mean_diff))
    if scatter_upper>=15: 
        if scatter_upper>(round(scatter_upper,-1)) :
            mean_range=[-round(scatter_upper,-1)-5,round(scatter_upper,-1)+5]
        else:
            mean_range=[-round(scatter_upper,-1),round(scatter_upper,-1)]
    elif scatter_upper<15 and scatter_upper>2:
        if scatter_upper > max(abs(min_mean_diff),max_mean_diff):
                mean_range=[-scatter_upper,scatter_upper]
        else:
                mean_range=[-scatter_upper-1,scatter_upper+1]
        if not (mean_range[1]/2).is_integer():
               mean_range =[ mean_range[0]-1,mean_range[1]+1 ]
    elif scatter_upper<=2 and scatter_upper>0:
        if scatter_upper > max(abs(min_mean_diff),max_mean_diff):
            mean_range=[-scatter_upper,scatter_upper]
        else:
            mean_range=[-scatter_upper-0.5,scatter_upper+0.5]
    else:
        print ("Difference calculated wrongly")
         
    ## Select upper boundary of contour colorbar#
    if max_stnd_dev > 3 and max_stnd_dev <= 6:
        std_dev_range=np.arange(0,max_stnd_dev+1,1)
    elif max_stnd_dev >6 and max_stnd_dev <=14:
        if (max_stnd_dev/2).is_integer():
            std_dev_range=np.arange(0,max_stnd_dev+2,2)
        else:
            std_dev_range=np.arange(0,max_stnd_dev+3,2)
    elif max_stnd_dev >= 0 and max_stnd_dev <= 1:
        std_dev_range=np.arange(0,max_stnd_dev+0.2,0.2)
    elif max_stnd_dev > 1 and max_stnd_dev <= 3:
        std_dev_range=np.arange(0,max_stnd_dev+0.5,0.5)     
    elif max_stnd_dev >= 14 and max_stnd_dev < 50:
        if max_stnd_dev > round(max_stnd_dev,-1):
            std_dev_range=np.arange(0,round(max_stnd_dev,-1)+5,5)
        else:
            std_dev_range=np.arange(0,round(max_stnd_dev,-1),5)
    else:
        print ("Standard Deviation is over ranged")
    
    print(
    "scale coloarbar scale is: ", mean_range,
    "coutour line scale is: ", std_dev_range)
    
    ## Use different color schemes for different variables    
    if metprm == 'pr' :
        color_map = 'PuOr'
        variable_name = 'Precipitation '
        scatter_name = (
            'Projected Ensemble Mean Change in Variable (%)')
        contour_name = (
            'Projected Ensemble Mean Change Standard Deviation in Variable (%)')
        D['mean_diff']=D.rel_diff
    elif metprm =='tasmax':
        color_map = 'RdYlBu_r'
        variable_name = 'Maximum Air Temperature '
        scatter_name = (
            'Projected Ensemble Mean Change in Variable (%)')
        contour_name = (
            'Projected Ensemble Mean Change Standard Deviation in Variable (%)')
        D['mean_diff']=D.raw_diff/100
        D['dev_diff']=D.dev_diff/100
    elif metprm == 'sfcWindmax':
        color_map = 'RdYlBu_r'
        variable_name = 'Maximum Wind Speed '
        scatter_name = (
            'Projected Ensemble Mean Change in Variable (%)')
        contour_name = (
            'Projected Ensemble Mean Change Standard Deviation in Variable (%)')
        D['mean_diff']=D.rel_diff
    elif metprm == 'sfcWind':
        color_map = 'RdYlBu_r'
        variable_name = 'Mean Wind Speed '
        scatter_name = (
            'Projected Ensemble Mean Change in Variable (%)')
        contour_name = (
            'Projected Ensemble Mean Change Standard Deviation in Variable (%)')
        mean_range = [-2.0,2.0]
        std_dev_range = [0, 2.0]
        D['mean_diff']=D.rel_diff
    elif metprm == 'clt':
        color_map = 'RdYlBu_r'
        variable_name = 'Total Cloud Fraction '
        scatter_name = (
            'Projected Ensemble Mean Change in Variable (%)')
        contour_name = (
            'Projected Ensemble Mean Change Standard Deviation in Variable (%)')
        D['mean_diff']=D.rel_diff
    elif metprm == 'rsds':
        color_map = 'RdYlBu_r'
        variable_name = 'Surface Solar Radiation '
        scatter_name = (
            'Projected Ensemble Mean Change in Variable (%)')
        contour_name = (
            'Projected Ensemble Mean Change Standard Deviation in Variable (%)')
        D['mean_diff']=D.rel_diff
    else:
        print('Do not include this parameter')

    ## Set region  
    if latlons : 
        min_lat = latlons[0]
        max_lat = latlons[1]
        min_lon = latlons[2]
        max_lon = latlons[3]
    else : 
        min_lat = D.lat.values.min()
        max_lat = D.lat.values.max()
        min_lon = D.lon.values.min()
        max_lon = D.lon.values.max()
        
    clon = min_lon + (max_lon - min_lon)/2.0
    clat = min_lat + (max_lat - min_lat)/2.0 
    
    ## Set subploting parameters
    if calc_annual :
        nr = 1
        nc = 1
        ne = 1
        wide = 24
        fig_name = 'Annaul '
    else :
        nr = 3
        nc = 4
        ne = 12
        wide = 28
        fig_name = 'Monthly '
        month_lab=[
            'January','February', 'March','April',
            'May','June','July','August',
            'September','October','November','December'
        ]
        month = range(1,13)

    print(variable_name,contour_name,scatter_name)

    ## Create plotting framework
    fig,axs = plt.subplots( 
        frameon=False,
        nrows=nr,
        ncols=nc,
        figsize=(wide, 18),
        subplot_kw={'xticks': [], 'yticks': []}) 


    fig.subplots_adjust(    
        left=0.01, right=0.90,
        top =0.95, bottom=0.05,
        hspace=0.06, wspace=0.05) 

    if calc_annual :
        for i in range (0,nr):              
            for j in range (0,nc):
                axs.spines['left'].set_visible(False)
                axs.spines['right'].set_visible(False)
                axs.spines['bottom'].set_visible(False)
                axs.spines['top'].set_visible(False)
    else : 
        for i in range (0,nr):              
            for j in range (0,nc):
                axs[i,j].spines['left'].set_visible(False)
                axs[i,j].spines['right'].set_visible(False)
                axs[i,j].spines['bottom'].set_visible(False)
                axs[i,j].spines['top'].set_visible(False)
    
    ## Draw maps
    for i in range(0,ne):
        chart = fig.add_subplot(  
            nr, nc, i+1,projection=cartopy.crs.LambertConformal(
                central_longitude=clon,central_latitude=clat))

        _ = chart.set_extent(
            [min_lon, max_lon, min_lat, max_lat], 
            crs=cartopy.crs.PlateCarree())
        ## Use coastlines instead of fancy background
        _ = chart.coastlines('50m')
      # _ = chart.add_image(stamen_terrain,8,interpolation='spline16')
   

        ## Add scatter plot with relative difference (color)
        ## and significant SD level (size)
        if calc_annual : 
            m01 = chart.scatter(
                D.lon,D.lat,
                transform=cartopy.crs.PlateCarree(),
                cmap='Oranges',    
                c=D.mean_diff.where(D.mean_diff>0)*100,
                s=D.sig_diff*120,
                marker = 6,     
                vmin=0,vmax=max(mean_range))
            m02= chart.scatter(
                D.lon,D.lat,
                transform=cartopy.crs.PlateCarree(),
                cmap='Blues_r',    
                c=D.mean_diff.where(D.mean_diff<=0)*100,
                s=D.sig_diff*120,
                marker = 7,     
                vmin=min(mean_range),vmax=0)    
        else : 
            m01 = chart.scatter(
                D.lon,D.lat,
                transform=cartopy.crs.PlateCarree(),
                cmap='Oranges',    
                c=D.mean_diff.sel(month=month[i])*100,
                s=D.sig_diff.sel(month=month[i])*5,    
                marker = 6,     
                vmin=0,vmax=max(mean_range))
            m02= chart.scatter(
                D.lon,D.lat,
                transform=cartopy.crs.PlateCarree(),
                cmap='Blues_r',    
                c=D.mean_diff.sel(month=month[i])*100,
                s=D.sig_diff.sel(month=month[i])*5,    
                marker = 7,     
                vmin=min(mean_range),vmax=0)
                
    fig.suptitle(variable_name+sim_name, fontsize=25)

    cmap=plt.get_cmap('Blues_r')
    norm=mpl.colors.Normalize (vmin=min(mean_range),vmax=0)
    ax_coutour=fig.add_axes([0.91, 0.05, 0.015, 0.45])
    cb = mpl.colorbar.ColorbarBase(ax_coutour,cmap = cmap,
	    norm=norm,ticks=[min(mean_range), 0])
    cb.ax.tick_params(labelsize=20)
	
    cmap=plt.get_cmap('Oranges')
    norm=mpl.colors.Normalize (vmin=0,vmax=max(mean_range))
    ax_coutour=fig.add_axes([0.91, 0.5, 0.015, 0.45])
    cb = mpl.colorbar.ColorbarBase(ax_coutour,cmap = cmap,
	    norm=norm, ticks=[0,max(mean_range)])
    cb.ax.tick_params(labelsize=20)
	    
    plt.figtext(
        0.95,0.78,variable_name+scatter_name,
        fontsize = 25, rotation ='vertical')  
    plt.savefig(fig_name+variable_name+sim_name+'_mean_change.png')
    plt.close()
    
    
    ''' Here, create the second (ensemble agreement) figure '''
    
    ## Create plotting framework
    fig,axs = plt.subplots( 
        frameon=False,
        nrows=nr,
        ncols=nc,
        figsize=(wide, 18),
        subplot_kw={'xticks': [], 'yticks': []}) 

    fig.subplots_adjust(    
        left=0.01, right=0.90,
        top =0.95, bottom=0.05,
        hspace=0.06, wspace=0.05) 

    if calc_annual :
        for i in range (0,nr):              
            for j in range (0,nc):
                axs.spines['left'].set_visible(False)
                axs.spines['right'].set_visible(False)
                axs.spines['bottom'].set_visible(False)
                axs.spines['top'].set_visible(False)
    else : 
        for i in range (0,nr):              
            for j in range (0,nc):
                axs[i,j].spines['left'].set_visible(False)
                axs[i,j].spines['right'].set_visible(False)
                axs[i,j].spines['bottom'].set_visible(False)
                axs[i,j].spines['top'].set_visible(False)
                
    for i in range(0,ne):
        ## Create map
        chart = fig.add_subplot(  
            nr, nc, i+1,projection=cartopy.crs.LambertConformal(
                central_longitude=clon,central_latitude=clat))

        _ = chart.set_extent(
            [min_lon, max_lon, min_lat, max_lat], 
            crs=cartopy.crs.PlateCarree())
        _ = chart.coastlines('50m')
        if show_dev :
            if calc_annual :              
                ag=D.ens_agr*100
                rd = D.dev_diff*100
            else:
                ag=D.ens_agr.sel(month=month[i])*100
                rd = D.dev_diff.sel(month=month[i])*100

            levels = [0, 60,70,80,90,100]
            ## Draw the ensemble standard deviation
            m1 = chart.scatter(
                D.lon,D.lat,
                transform=cartopy.crs.PlateCarree(),
                cmap='cool',    
                c=rd,
                s=rd*100,    
                vmin=0,vmax=max(std_dev_range))
            ## Hatch by ensemble agreement 
            m2 = ag.plot.contourf(
                'lon','lat',ax=chart,
                transform=cartopy.crs.PlateCarree(),
                levels=levels,hatches=["","/","*","o","x"],
                colors='none', alpha=0,
                add_colorbar=False)    
            cmap=plt.get_cmap('cool')
            norm=mpl.colors.Normalize (vmin=0,vmax=max(std_dev_range))
            ax_coutour=fig.add_axes([0.91, 0.05, 0.015, 0.9])
            cb = mpl.colorbar.ColorbarBase(ax_coutour,cmap = cmap,
                norm=norm,ticks=[0,max(std_dev_range)])
            cb.ax.tick_params(labelsize=20)
            plt.figtext(
                0.95,0.925,variable_name+contour_name,
                fontsize=25,rotation='vertical')
    fig.suptitle(variable_name+sim_name, fontsize=25)
    hatch_description = (
        "hatching = different percentage ensemble agreement"
        + " on directions of changes\n"
        + "60%-70% represented by /, 70%-80% by *, "
        + "80%-90% by o , and 90%-100% by x")
    plt.figtext(
        0.2,0.01,hatch_description,
        fontsize = 25, rotation ='horizontal')   
    plt.savefig(fig_name+variable_name+sim_name+'_standard_deviation.png')
    plt.close()
    
# ________________________________________________________________________
# ::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::

''' Pull data
    ---------
    Data paths and file names will vary. 
                                                                             '''
                                                                             
#| > NEED TO THINK ABOUT WHAT SHOULD BE EXTERNAL ARGUMENTS <- |#
#|   Currently have a lot of hard coded variables, while      |#
#|   still keeping some external arguments. This is useful    |#
#|   for testing, but makes general use harder to describe    |#

datapath = '/home/ubuntu/eucordex/NorthSea'
chart_sim = False
report_range = False
calc_annual = True

''' Different met-parameters will have different associated aggregations. 
                                                                             '''
## ... Set storage ...
fname_hsim = []
fname_psim = []
matrix_channel = [[0,0,0,0]] 
matrix_stats   = [[0,0,0,0]] 

if metprm == 'pr' :
    aggr = 'monsum_'
elif metprm =='tasmax':
#    aggr = 'monmax_'
    aggr = 'monmean_'
elif metprm == 'rsds':
    aggr = 'monmean_'
elif metprm== 'clt':
    aggr = 'monmean_'
elif metprm =='sfcWindmax':
    aggr = ''
elif metprm =='sfcWind':
    aggr = 'monmean_'
else :
    aggr = ''
    
''' Selection of sub-ensembles
    --------------------------
    ** If we're going to code these sort of decisions here
       then should probably disgard all extrnal arguments
       and instead just have the script be the functions (above)
       and a potential "main" use case (this portion)?. **
                                                                  '''
ens_A = ['ICHEC-EC-EARTH','MOHC-HadGEM2-ES','NCC-NorESM1-M']
ens_B = ['CNRM-CERFACS-CNRM-CM5','IPSL-IPSL-CM5A-MR','MPI-M-MPI-ESM-LR']    

## ... Grab different simulations ...
# -> HERE HARD CODE THE EU-CORDEX HEADER, SHOULD MAKE IT FLEXIBLE <-
if ens_range == 'all':
    fname_hsim += grod.glob(
        datapath + '/'
        + aggr + 'NorthSea_'
        + metprm+ '_EUR-11_*_historical_*.nc')
    fname_psim += grod.glob(
        datapath + '/'
        + aggr + 'NorthSea_'
        + metprm+ '_EUR-11_*_rcp85_*.nc')
elif ens_range == 'A':
    for dri_mod in ens_A:
        fname_hsim += grod.glob(
            datapath + '/'
            + aggr + 'NorthSea_'
            + metprm+ '_EUR-11_'+dri_mod+'_historical_*.nc')
        fname_psim += grod.glob(
            datapath + '/'
            + aggr + 'NorthSea_'
            + metprm+ '_EUR-11_'+dri_mod+'_rcp85_*.nc')
elif ens_range == 'B':
    for dri_mod in ens_B:
        fname_hsim += grod.glob(
            datapath + '/'
            + aggr + 'NorthSea_'
            + metprm+ '_EUR-11_'+dri_mod+'_historical_*.nc')
        fname_psim += grod.glob(
            datapath + '/'
            + aggr + 'NorthSea_'
            + metprm+ '_EUR-11_'+dri_mod+'_rcp85_*.nc') 
print (len(fname_hsim))

## ... Make sequence of historical and rcp85 files ...
fname_hsim.sort()                   
fname_psim.sort()

''' Put together file header data and use that to check that
    historical and future data are in matching order of generating
    GCM-RCM.                  
    ** This would be handy as it's own generic function **
                                                                             '''
if metprm == 'sfcWindmax':
    headers_h = np.array(
        [('_'.join(x.split('_')[1:4])+'_'.join(x.split('_')[6:]))
         for x in fname_hsim])
    headers_p = np.array(
        [('_'.join(x.split('_')[1:4])+'_'.join(x.split('_')[6:])) 
         for x in fname_psim])
else:
    headers_h = np.array(
        [('_'.join(x.split('_')[2:5])+'_'.join(x.split('_')[7:])) 
         for x in      fname_hsim])
    headers_p = np.array(
        [('_'.join(x.split('_')[2:5])+'_'.join(x.split('_')[7:])) 
         for x in fname_psim])

''' If there is a desired region to draw a map of then set `graph_range`
    input seq: min_lat,max_lat,min_lon,max_lon 
                                                                             '''
graph_range=[50.0,54.0,-2.0,6.0] 

''' While examining individual simulations keep track of some values so
    that can calculate some ensmeble stats
                                                                             '''
ens   = []                        # create list of datasets

## ... Loop to process simulations one by one ...
idx = 0  
while idx < len(fname_hsim) :
    if not headers_h[idx] == headers_p[idx]:
        print( "++ unmatched history and project data ++")
        break
    print('processing ', fname_hsim[idx], fname_psim[idx])
    
    ## ... Calculate climatology stats ...
    D = set_values(
        hsim_path=fname_hsim[idx],
        psim_path=fname_psim[idx],
        metprm=metprm,
        calc_annual=calc_annual)
    D = regrid(D,'nn',datapath+'/'+'base_grid.txt')
    if metprm =='tasmax':
        D['val_diff']=D.raw_diff
    else: 
        D['val_diff']=D.rel_diff
        
    ## ... Track ensemble values for relative diff and raw diff ...
    x = list(D.data_vars)
    x.remove('rel_diff')
    x.remove('raw_diff')
    X = D.drop(x)
    # -> ASSUMING THAT FIELDS HAVE NO VERTICAL COMPONENTS <- 
    if 'height' in list(X.coords) :
        X=X.drop('height')
    # dlist += [X]
    ens += [regrid(X,'nn',datapath+'/'+'base_grid.txt')]   # use common grid
    del(X)    

    ## ... Record value ranges ...
    if report_range : 
        matrix_stats = np.append(
            matrix_stats,[show_range(D,None)],axis=0)
        matrix_channel = np.append(
            matrix_channel,[show_range(D,graph_range)],axis=0)
    
    ## ... Create charts for each simulation ...
    if chart_sim : 
        map_simulation_values(D,fname_psim[idx],graph_range)

    idx += 1    

''' Next step is to calculate some statistics over the stored ensemble
    values                                                                            '''

E = ens[0].copy(deep=True)
E.raw_diff.values = np.nanmean(
    np.array([X.raw_diff.values for X in ens]),axis=0)
E.rel_diff.values = np.nanmean(
        np.array([X.rel_diff.values for X in ens]),axis=0)
        
''' Here we select raw difference (Projected-Historial) for tas, 
    reletive difference (Proj-His)/His for other metprm
                                                                    '''
if metprm =='tasmax':
    E['val_diff']=E.raw_diff
    for X in ens:
        X['val_diff']=X.raw_diff
    print ("pass")
else: 
    E['val_diff']=E.rel_diff
    for X in ens:
        X['val_diff']=X.rel_diff 

''' If going to calculate annual values, there is whole year value/no month 
    for sig_diff; if for monthly, sig_diff is calcuated by monthly
                                                                                    '''
if calc_annual :
    E['dev_diff'] = E.val_diff.copy(deep=True)
    E.dev_diff.values = np.nanstd(
        np.array([X.val_diff.values for X in ens]),axis=0)
    E['err_diff'] =  E.dev_diff/np.sqrt(len(ens))
    E['sig_diff'] = E.err_diff.copy(deep=True)
    E['sig_diff'].values = (E.sig_diff.values*0)+4  
    for sig_lev in range(4,0,-1) : 
        E['sig_diff'].values[
            (np.abs(E.val_diff)
             <= sig_lev*E.err_diff)
        ] = sig_lev
## calculate ensemble agreement values
    E['ens_agr'] = E.sig_diff.copy(deep=True)
    E['ens_agr'].values = (E.sig_diff.values*0)
    for X in ens:
        E.ens_agr.values[X.val_diff.values>0]+=1
    E.ens_agr.values[E.ens_agr<(len(ens)/2)
        ]=len(ens)-E.ens_agr.values[E.ens_agr
        <(len(ens)/2)]
   
    E.ens_agr.values=E.ens_agr.values/len(ens)
else : 
    E['dev_diff'] = E.val_diff.copy(deep=True)
    E.dev_diff.values = np.nanstd(
        np.array([X.val_diff.values for X in ens]),axis=0)
    E['err_diff'] =  E.dev_diff/np.sqrt(len(ens))
    E['sig_diff'] = E.err_diff.copy(deep=True)
    E['sig_diff'].values = (E.sig_diff.values*0)+4
    E['ens_agr'] = E.sig_diff.copy(deep=True)
    E['ens_agr'].values = (E.sig_diff.values*0)   
    for m in range(1,13) :
        for sig_lev in range(4,0,-1) : 
            E['sig_diff'].sel(month=m).values[
                (np.abs(E.val_diff.sel(month=m))
                <= sig_lev*E.err_diff.sel(month=m))
                ] = sig_lev    
        for X in ens:
            E.ens_agr.sel(month=m).values[
                X.val_diff.sel(month=m).values
                >0]+=1
        E.ens_agr.sel(month=m).values[E.ens_agr.sel(month=m)<(len(ens)/2)
            ]=len(ens)-E.sel(month=m).ens_agr.values[E.ens_agr.sel(month=m)
            <(len(ens)/2)]
    E.ens_agr.values=E.ens_agr.values/len(ens)
     
          
R=show_range(E,graph_range)
plot_label = (
    'Projected Change in Annual Variable from'
    + '1976-2005 to 2021 - 2050_'
    + ens_range
)
map_ensemble_values(
    E,R,graph_range,metprm,show_dev=True,
    sim_name=plot_label,
    calc_annual=calc_annual)


# ________________________________________________________________________    
# ::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::

''' Extract the statistics results of different regions (eg.,
    NorthSea, The Channel) to help chose levels for colorbars in
    graphics                                                                 '''

if report_range :
    
    matrix_stats = np.delete(                        # delete the fake row 
        matrix_stats,0,axis=0)  
    df = pd.DataFrame(                               # make a dataset to save 
        columns=[                                    # columns names
            'min_mean_diff','max_mean_diff',
            'min_stnd_dev','max_stnd_dev'])
    for idx in range(0,len(matrix_stats)):
        df.loc[idx] = matrix_stats[idx]              # write each simulation
                                                     # stats results to dataset

    df.to_csv(                                       # save dataset as csv
        r'stats_'+metprm+'.csv',index=False)       

    matrix_channel = np.delete(                      # delete the fake row
        matrix_channel,0,axis=0)   
    df = pd.DataFrame(                               # make a dataset to save 
        columns=[                                    # columns names
            'min_mean_diff','max_mean_diff',
            'min_stnd_dev','max_stnd_dev'])      
    for idx in range(0,len(matrix_channel)):
            df.loc[idx] = matrix_channel[idx]        # write each simulation
                                                     # statistics results
                                                     # to dataset
	
    df.to_csv(                                       # save dataset as csv
        r'stats_channel_'+metprm+'.csv',
        index=False)       



